<?php

if(!function_exists('lazer')) {
    function lazer($data, $category = 'Default') {
        $client = new \GuzzleHttp\Client();
        $url = isset($_ENV['LAZER_URL']) ? $_ENV['LAZER_URL'] : 'http://lazer:4004/data';
        $client->post($url,
		[
			'json' => [
				'data' => $data,
				'stacktrace' => debug_backtrace(),
				'category' => $category,
			],
		]);
    }
}